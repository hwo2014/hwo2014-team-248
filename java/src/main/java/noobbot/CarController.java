package noobbot;

import serverMessages.carPositions.CarPositions;
import serverMessages.crash.Crash;
import serverMessages.gameInit.Piece;
import serverMessages.gameInit.Track;
import serverMessages.lapFinished.LapFinished;
import serverMessages.spawn.Spawn;

import java.util.*;

/**
 * Created by Tom on 24/04/2014.
 */
public class CarController {

    private HashMap<Piece, SwitchDirection> laneSwitches = new HashMap<>();

    private boolean lastResortTurboDone = false;

    private String color= "";

    private Track track;
    private int currentTick=0;

    private int pieceIndex = -1;
    private double lastPieceDist = 0.0;
    private double remainPieceDist = 0.0;
    private double maxDriftAngle = 0.0;
    private double prevDriftAngle = 0.0;
    private double maxSegmentDriftAngle = 0.0;
    private double maxLapDriftAngle = 0.0;
    private double allTimeMaxLapDrift = 0.0;
    private int currentLap = 0;

    private int segmentIndex =0;
    private int prevSegmentIndex = -1;
    private double segmentDistance = 0.0;

    private PidAlgorithm pidAlgorithm = null;
    private float PID_P = 5;
    private float PID_I = 0.02f;
    private float PID_D = 0;
    private float targetVel = 5;

    private boolean learning = true;
    private double maxLearningSpeed = 3.0;
    private double driftFriction = 0.32;
    private double tweek = 1.2;
    private double lastKnownGood = 1.0;
    private int crashTweek = 1;
    private boolean liveTweek = true;
    private int lastCrashlap = -1;
    private double dragConstant = 0.02;

    int laneIndex = 0;

    private boolean boost = false;
    private boolean driftProtection = false;

    private double maxVelocity = 0;
    private boolean velocityReached = false;
    private int startDelTick = 0;

    private boolean breaking = false; //used for debug message only

    private int dTicks = 30;

    private int learningOffset =0;
    private double throttle = 1.0;
    private double acceleration = 0.0;

    private SwitchLane savedSwitch =null;

    public CarController(String color) {
        this.color = color;

        //init PID controller
        pidAlgorithm = new PidAlgorithm((float)maxLearningSpeed, 0, PID_P, PID_I, PID_D);

    }

    public void setTrack(Track track){
        this.track = track;
    }

    public ClientMessage getAction(CarPositions carPositions){

        int carIndex = carPositions.getCarIdByColour(color);
        pieceIndex = carPositions.currentPiece(carIndex);

        laneIndex = carPositions.getCurrentLane(carIndex);

        double driftAngle = carPositions.getAngleByIndex(carIndex);
        double pieceDistance = carPositions.getPieceDistance(carIndex);

        track.updatePiece(pieceIndex);

        if (carPositions.getGameTick() != null) {
            //First message won't have a tick
            currentTick = carPositions.getGameTick();
        } else {
            track.getCurrentPiece().setStart();
            calculateBasicSwitchTargets();
        }

        //velocity (unit per tick)
        if(track.pieceChanged()) lastPieceDist = -remainPieceDist;
        double velocity = pieceDistance - lastPieceDist;
        segmentDistance = track.segmentChanged() ? pieceDistance : (segmentDistance +=  (pieceDistance - lastPieceDist));

        lastPieceDist = pieceDistance;
        remainPieceDist = track.getCurrentPieceDistanceRem(pieceDistance, laneIndex);

        segmentIndex = track.getSegmentIndex(pieceIndex);

        double changeInDriftAngle = driftAngle - prevDriftAngle;

        double currRadius = track.getCurrentPieceRadius(laneIndex);
        //System.out.println(currentTick + "," + maxLearningSpeed + "," + throttle + "," + velocity + "," + pieceIndex + "," + currRadius + "," + driftAngle + "," + changeInDriftAngle);
        //System.out.println(currentTick + "," + throttle + "," + velocity + ","  + currRadius + "," + driftAngle);
        //System.out.println(currentTick + "," + throttle + "," + velocity + ","  + currRadius + "," + driftAngle + " segment: " + segmentIndex + " piece: " + pieceIndex);
        //end of Graph


        if(learning){
            if(currentTick<(3+learningOffset)) {
                switch (currentTick - learningOffset) {
                    case 0:
                        if (velocity > 0.0) {
                            learningOffset++;
                            throttle = 0;
                        }else{
                            throttle = 1;
                        }

                        break;

                    case 1:
                        if(velocity==0.0){
                            learningOffset++;
                        }else {
                            acceleration = velocity;
                            System.out.println("Accel: " + acceleration + " tick: " + currentTick + "," + learningOffset);
                        }
                        throttle = 1;
                        break;


                    case 2:
                        dragConstant = 1 - (velocity - acceleration) / acceleration;
                        System.out.println("dragConstant: " + dragConstant + " velocity: " + velocity + " tick: "+currentTick + "," + learningOffset);
                        throttle = 1;
                        break;

                    default:
                        break;

                }

            }else {

                if (track.isCurrentSegmentBend()) {
                    if (driftAngle == 0) {

                        maxLearningSpeed+=0.1;

                        if (track.getNextSegment().isBend() && track.getCurrentSegmentAdjustedRadius(laneIndex) > track.getNextSegment().getAdjustedRadius(track.getDistanceFromCenter(laneIndex))) {
                            //driftFriction = calcDriftFriction(velocity, track.getCurrentSegmentAdjustedRadius(laneIndex), 0);
                            double nextVel = calcCornerSpeed(track.getNextSegment().getAdjustedRadius(track.getDistanceFromCenter(laneIndex)), driftFriction, 1);
                            double brakingDistance = getBrakingDistance(velocity, nextVel);
                            double distToNextSegment = track.getCurrentSegmentAdjustedLength(laneIndex) - segmentDistance;
                            if (brakingDistance > 0 && distToNextSegment <= brakingDistance) {
                                maxLearningSpeed = nextVel;
                            }
                        }

                    } else {

                        driftFriction = calcDriftFriction(velocity,track.getCurrentSegmentAdjustedRadius(laneIndex),Math.abs(driftAngle));
                        System.out.println("DriftFriction: " + driftFriction + " ,DriftAngle: " + driftAngle + " ,Velocity: " + velocity + ",Segment: " + segmentIndex);
                        learning = false;
                    }
                }

                if (track.segmentChanged()) {
                    if (prevSegmentIndex > -1 && track.getSegmentByIndex(prevSegmentIndex).isBend()) {
                        if(!track.isCurrentSegmentBend()) {
                            driftFriction = calcDriftFriction(velocity, track.getSegmentByIndex(prevSegmentIndex).getAdjustedRadius(track.getDistanceFromCenter(laneIndex)), 0);
                            maxLearningSpeed = calcCornerSpeed(track.getNextSegment().getAdjustedRadius(track.getDistanceFromCenter(laneIndex)), driftFriction, 1);
                        }
                    }

                }

                if (pidAlgorithm.getMode() == PID.Mode.MANUAL) pidAlgorithm.setMode(PID.Mode.AUTOMATIC);
                pidAlgorithm.setTarget((float) maxLearningSpeed);
                pidAlgorithm.compute((float) velocity, currentTick);
                throttle = pidAlgorithm.getOutput();
            }

        }else {

            if (driftAngle > maxSegmentDriftAngle) {
                maxSegmentDriftAngle = driftAngle;
            }
            if(maxSegmentDriftAngle > maxLapDriftAngle){
                maxLapDriftAngle = maxSegmentDriftAngle;
            }

            if(track.segmentChanged()) {
                breaking = false; //used for debug message only
                //if (prevSegmentIndex > -1 && track.getSegmentByIndex(prevSegmentIndex).isBend()) {
                    System.out.println("Segment (" + prevSegmentIndex + ") Velocity: " + velocity + ", Max Drift: " + maxSegmentDriftAngle+ ", Radius: "+ track.getSegmentAdjustedRadius(prevSegmentIndex, laneIndex) + ", Length: "+ track.getSegmentAdjustedLength(prevSegmentIndex, laneIndex));
                    if(track.getSegmentByIndex(prevSegmentIndex).isBend()){
                        if(boost && maxSegmentDriftAngle<30){
                            track.getSegmentByIndex(prevSegmentIndex).increaseBoost(0.5);
                            System.out.println("Boost!");
                        }
                    }

                    if(currentLap != track.getLap()){
                        double addTweek = 0.0;
                        if(liveTweek && lastCrashlap != currentLap) {
                            lastKnownGood = tweek;
                            //if (maxLapDriftAngle > allTimeMaxLapDrift) {
                                if (maxLapDriftAngle < 20) {
                                    addTweek += 0.3;
                                } else if (maxLapDriftAngle < 30) {
                                    addTweek += 0.2;
                                } else if (maxLapDriftAngle < 40) {
                                    addTweek += 0.1;
                                } else if (maxLapDriftAngle < 50) {
                                    addTweek += 0.05;
                                } else if (maxLapDriftAngle < 55) {
                                    addTweek += 0.01;
                                }

                                addTweek = addTweek/crashTweek;
                                tweek += addTweek;
                                allTimeMaxLapDrift = maxLapDriftAngle;
                                System.out.print("+");
                            //}
                        }
                        currentLap = track.getLap();
                        System.out.println("Max Lap Drift: " + maxLapDriftAngle + " Tweek: " + tweek + ", Added Tweek: " + addTweek);
                        maxLapDriftAngle = 0;
                    }

                    maxSegmentDriftAngle = 0;
                //}

            }

            Segment currentSegment = track.getSegmentByIndex(segmentIndex);

            if (currentSegment.isBend()) { // in corner

                //if (track.currentSegmentApexReached(segmentDistance, laneIndex)) {
                targetVel = (float) Math.sqrt(driftFriction * track.getCurrentSegmentAdjustedRadius(laneIndex) * tweek);
                if (pidAlgorithm.getMode() == PID.Mode.MANUAL) pidAlgorithm.setMode(PID.Mode.AUTOMATIC);
                pidAlgorithm.setTarget(targetVel);
                pidAlgorithm.compute((float) velocity, currentTick);
                throttle = pidAlgorithm.getOutput();

                /*if (track.getNextSegment().isBend() && currentSegment.getAdjustedRadius(track.getDistanceFromCenter(laneIndex)) > track.getNextSegment().getAdjustedRadius(track.getDistanceFromCenter(laneIndex))) {
                    double linkedTurnRadius = track.getNextSegment().getAdjustedRadius(track.getDistanceFromCenter(laneIndex));
                    double nextEntryVelocity = calcCornerSpeed(linkedTurnRadius, driftFriction, tweek) + track.getNextSegment().getBoost();
                    double brakingDistance = getBrakingDistance(velocity, nextEntryVelocity);
                    double distToNextSegment = track.getCurrentSegmentAdjustedLength(laneIndex) - segmentDistance;
                    if (brakingDistance > 0 && distToNextSegment <= brakingDistance) {
                        //System.out.println("Distance to next:" + distToNextSegment + "braking distance:" + brakingDistance + "segment Distance: " + segmentDistance);
                        pidAlgorithm.setMode(PID.Mode.MANUAL);
                        throttle = 0.0;
                    }
                }*/
                Segment nextCornerSegment = track.getNextCornerSegment();
                //int nextLane = predictLane(pieceIndex, track.getNextCornerPiece(pieceIndex), laneIndex);
                //if(nextLane != laneIndex) System.out.println("Lane will change before corner: " + laneIndex + ", " + nextLane);
                double linkedTurnRadius = nextCornerSegment.getAdjustedRadius(track.getDistanceFromCenter(laneIndex));
                double nextEntryVelocity = calcCornerSpeed(linkedTurnRadius, driftFriction, tweek);
                double brakingDistance = getBrakingDistance(velocity, nextEntryVelocity);
                double distToNextCornerSegment = track.getDistToNextCornerSegment() + track.getCurrentSegmentAdjustedLength(laneIndex) - segmentDistance;
                if (brakingDistance > 0 && distToNextCornerSegment <= brakingDistance) {
                    if (!breaking) System.out.println("[Corner] Distance to next (" + track.getSegmentIndex(nextCornerSegment) + "): " + distToNextCornerSegment + ", braking distance: " + brakingDistance + ", segment Distance: " + segmentDistance);
                    breaking = true;
                    pidAlgorithm.setMode(PID.Mode.MANUAL);
                    throttle = 0.0;
                }else{
                    System.out.println("PID");
                }

            } else { //in straight, go to wrap speed. well until we have to brake

                pidAlgorithm.setMode(PID.Mode.MANUAL);
                //int nextLane = predictLane(pieceIndex, track.getNextCornerPiece(pieceIndex), laneIndex);
                //if(nextLane != laneIndex) System.out.println("Lane will change before corner: " + laneIndex + ", " + nextLane);
                double nextRadius = track.getNextSegment().getAdjustedRadius(track.getDistanceFromCenter(laneIndex));
                double nextMaxSpeed = calcCornerSpeed(nextRadius, driftFriction, tweek) + track.getNextSegment().getBoost();
                double brakingDistance = getBrakingDistance(velocity, nextMaxSpeed);

                double dist = track.getCurrentSegmentAdjustedLength(laneIndex)-segmentDistance;
                //System.out.println("segment index: " + segmentIndex + ", velocity:" + velocity + ", Next Max Speed: " + nextMaxSpeed + ", braking distance: " + brakingDistance + ", distance: " + dist);
                if (brakingDistance > 0 && dist <= brakingDistance) {
                    if (!breaking) System.out.println("Distance to next (" + track.getNextSegmentIndex() + "): " + dist + ", braking distance: " + brakingDistance + ", segment Distance: " + segmentDistance);
                    breaking = true;
                    //System.out.println("Segment Length (" + segmentIndex + ") : "+ track.getCurrentSegmentAdjustedLength(laneIndex) + "Distance to next: " + dist + "braking distance: " + brakingDistance );
                    //System.out.println("segment index: " + segmentIndex + ", velocity:" + velocity + ", Next Max Speed: " + nextMaxSpeed + ", braking distance: " + brakingDistance + ", distance: " + dist);
                    throttle = 0.0;
                } else {
                    throttle = 1.0;
                }

            }


            prevDriftAngle = driftAngle;
        }

        prevSegmentIndex = segmentIndex;


        if (track.pieceChanged()){
            //System.out.println("Distance to switch: " + distanceToSwitch(track.getCurrentPiece(), laneIndex) + remainPieceDist);

            /*
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("Segment ID: " + track.getCurrentPiece().getSegment().toString());
            System.out.println("Total distance: " + track.getCurrentPiece().getSegment().getAdjustedLength(track.getLanes().get(laneIndex).getDistanceFromCenter()));
            System.out.println("Segment has switch: " + track.getCurrentPiece().getSegment().canSwitch());
            System.out.println("Segment has n switches: " + track.getCurrentPiece().getSegment().getSwitchPieces().size());
            System.out.println("Piece has switch: " + track.getCurrentPiece().getSwitch());
            System.out.println("Distance to switch: " + track.getCurrentPiece().getSegment().distanceToSwitch(track.getCurrentPiece(), track.getLanes().get(laneIndex).getDistanceFromCenter()));
            System.out.println("Distance to end: " + track.getCurrentPiece().getSegment().distanceToEnd(track.getCurrentPiece(), track.getLanes().get(laneIndex).getDistanceFromCenter()));
            */
        }
        if(savedSwitch !=null){
            SwitchLane s = savedSwitch;
            savedSwitch = null;
            return s;
        }

        if(track.pieceChanged()) {
            SwitchDirection switchDirection = shouldSwitch();

            if(switchDirection != null){
                savedSwitch = new SwitchLane(switchDirection);
            }
            if(currentTick>1 && savedSwitch !=null){
                System.out.println("S");
                return savedSwitch;
            }
        }

        Segment currentSegment = track.getCurrentPiece().getSegment();
        if (track.isFinalLdap() && currentSegment.isBeforeStartLine(track.getCurrentPiece()) && !lastResortTurboDone){
            //Pre-send turbo message, it will be used as soon as we get one
            lastResortTurboDone = true;
            return new Turbo("Turbo!");
        }

        if (turboAvailable && !lastResortTurboDone){

            List<Segment> ls = track.getLongestSegments();
            Segment longestSeg = ls.get(0);

            if (longestSeg.getFirstPiece().equals(track.getCurrentPiece())){
                turboAvailable = false;
                return new Turbo("Turbo!");
            }
        }

        //Think your algo already does this but wanted it for testing anyway..
        if (track.getCurrentPiece().getSegment().containsStartLine() && track.isFinalLdap() && track.getCurrentPiece().getSegment().isBeforeStartLine(track.getCurrentPiece())){
            return new Throttle(1.0);
        }
        System.out.println("T"+throttle);
        return new Throttle(throttle);
    }


    private boolean turboAvailable = false;

    public void setTurboAvailable() {
        turboAvailable = true;
    }

    private boolean onTrack = true;

    public boolean isOnTrack(){
        return onTrack;
    }

    private double calcDriftFriction(double velocity, double radius, double driftAngle){
        double slip = Math.toDegrees(velocity / radius) - driftAngle;
        double thresholdV = Math.toRadians(slip) * radius;
        double driftFriction = ((thresholdV * thresholdV) / radius);
        return driftFriction;
    }

    private double calcCornerSpeed(double radius, double driftFriction, double tweek){
        if(Math.abs(radius) < 70 ) tweek *= 0.9;
        return Math.sqrt(driftFriction * radius * tweek) ;
    }

    private double getBrakingDistance(double currentVelocity, double requiredVelocity){
        double derivativeVel = currentVelocity;
        double distance=0;
        while(derivativeVel>requiredVelocity){
            derivativeVel -= dragConstant * derivativeVel;
            distance+=derivativeVel;
        }
        return distance;
    }

    public void parseCrashMessage(Crash crash){
        if (crash.getData().getColor().equals(this.color)){
            turboAvailable = false;
            onTrack = false;
            System.out.println("We've crashed...");
            if(liveTweek){
                if(lastKnownGood == tweek) lastKnownGood -= 0.1;
                tweek = lastKnownGood;
                crashTweek *= 2;
                lastCrashlap = currentLap;
            }
        }
    }

    public void parseSpawnMessage(Spawn spawn) {
        if (spawn.getData().getColor().equals(this.color)){
            onTrack = true;
            System.out.println("We're on the track again...");
        }
    }

    public void parseLapCompleted(LapFinished lapFinished) {
        if (lapFinished.getData().getCar().getColor().equals(this.color)){
            System.out.println("Lap: " + lapFinished.getData().getLapTime().getLap().toString() + ", Time: " + lapFinished.getData().getLapTime().getMillis().toString() + ", Ranking: " + lapFinished.getData().getRanking().getOverall().toString());
            track.lapCompleted();
        }
    }

    //Doesn't work well on 3/4 lanes
    public void calculateBasicSwitchTargets(){
        Piece firstSwitch = track.getFirstSwitch();
        HashMap<Piece, SwitchDirection> tmpSwitches = new HashMap<>();
        int currentLane = laneIndex;
        //System.out.println("Lane: " + currentLane + "\tSize: " + tmpSwitches.size());


        //hack for first switch..
        {

            List<Integer> possibleLanes = getPossibleLanes(currentLane);

            int bestLane = -1;
            double distance = -1;
            for (int possibleLane : possibleLanes) {
                double newDistance = track.getDistanceOfFirstCorners(possibleLane);

                if (newDistance < distance || distance == -1){
                    distance = newDistance;
                    bestLane = possibleLane;
                }
            }

            if (bestLane != currentLane){
                tmpSwitches.put(firstSwitch, switchDirection(currentLane, bestLane));
            }

            currentLane = bestLane;
            //System.out.println("Lane: " + currentLane + "\tSize: " + tmpSwitches.size() + "Piece: " + track.getPieceIndex(firstSwitch));

        }


        //Rest of the track...
        boolean done = false;
        Piece currentSwitch = track.getNextSwitch(firstSwitch);
        while (!done){
            List<Integer> possibleLanes = getPossibleLanes(currentLane);

            int bestLane = -1;
            double distance = -1;
            for (int possibleLane : possibleLanes) {
                double newDistance = track.distanceToSwitch(currentSwitch, possibleLane);

                if (newDistance < distance || distance == -1){
                    distance = newDistance;
                    bestLane = possibleLane;
                }
            }

            //System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");

            if (currentLane != bestLane){
                if (tmpSwitches.containsKey(currentSwitch)){
                    tmpSwitches.replace(currentSwitch, switchDirection(currentLane, bestLane));
                } else {
                    tmpSwitches.put(currentSwitch, switchDirection(currentLane, bestLane));
                }

            }

            currentLane = bestLane;

            //System.out.println("Lane: " + currentLane + "\tSize: " + tmpSwitches.size() + "Piece: " + track.getPieceIndex(currentSwitch));

            currentSwitch = track.getNextSwitch(currentSwitch);

            if (currentSwitch == null){
                done = true;
            }
        }

        this.laneSwitches = tmpSwitches;
    }

    public SwitchDirection shouldSwitch(){
        Piece next = track.getNextPiece();
        if (laneSwitches.containsKey(next)){
            return laneSwitches.get(next);
        }
        return null;
    }

    public int predictLane(int currentPieceIdx, int endPieceIndex, int currentLane){
        Piece next = null;
        if(currentPieceIdx == endPieceIndex) return currentLane;
        do {
            next = track.getNextPiece(currentPieceIdx);
            if (laneSwitches.containsKey(next)) {
                SwitchDirection switchD = laneSwitches.get(next);
                if (switchD.getSwitchDirection() == "Left") {
                    currentLane -= 1;
                } else {
                    currentLane += 1;
                }
            }
        }while(track.getPieceIndex(next) != endPieceIndex);

        if(currentLane < 0){
            return 0;
        }
        if(currentLane > track.getLanes().size()-1){
            return track.getLanes().size()-1;
        }
        return currentLane;
    }

    public SwitchDirection switchDirection(int currentLane, int targetLane){
        if (currentLane < targetLane){
            return SwitchDirection.SWITCH_DIRECTION_RIGHT;
        } else if (currentLane > targetLane) {
            return SwitchDirection.SWITCH_DIRECTION_LEFT;
        }
        return null;
    }

    public List<Integer> getPossibleLanes(int lane){
        List<Integer> lanes = new ArrayList<>();

        if (isValidLane(lane)){
            lanes.add(lane);
        }

        if (isValidLane(lane - 1)){
            lanes.add(lane - 1);
        }

        if (isValidLane(lane + 1)){
            lanes.add(lane + 1);
        }

        return lanes;
    }

    private boolean isValidLane(int lane){
        int amountOfLanes = track.getLanes().size() - 1;//valid lanes starting at 0

        if (amountOfLanes < 0){
            return false;
        }

        if (lane < 0 || lane > amountOfLanes){
            return false;
        }
        return true;
    }

    public void reset(){
        lastResortTurboDone = false;

        currentTick=0;

        pieceIndex = -1;
        lastPieceDist = 0.0;
        remainPieceDist = 0.0;
        maxDriftAngle = 0.0;
        prevDriftAngle = 0.0;
        maxSegmentDriftAngle = 0.0;
        maxLapDriftAngle = 0.0;
        currentLap = 0;

        segmentIndex =0;
        prevSegmentIndex = -1;
        segmentDistance = 0.0;

        lastCrashlap =-1;
        liveTweek=false;
        tweek=lastKnownGood;

        //learning = true;
        pidAlgorithm = new PidAlgorithm((float)maxLearningSpeed, 0, PID_P, PID_I, PID_D);
    }

}
