package noobbot;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

abstract class ClientMessage {
    public String toJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        if (gameTick != null){
            try {
                return (new ObjectMapper()).writeValueAsString(new Message(type(), data(), gameTick()));
            } catch (JsonProcessingException e) {
                System.out.println("Failed to create json from object! - 3");
                return null;
            }
        } else {
            try {
                return (new ObjectMapper()).writeValueAsString(new Message(type(), data()));
            } catch (JsonProcessingException e) {
                System.out.println("Failed to create json from object! - 2");
                e.printStackTrace();
                return null;
            }
        }
    }

    protected abstract Object data();

    protected abstract String type();

    public Integer gameTick = null;

    public final void setGameTick(Integer gameTick){
        this.gameTick = gameTick;
    }
    public final Integer gameTick() { return gameTick; }
}

class Join extends ClientMessage {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    protected Object data() { return this; }

    @Override
    protected String type() {
        return "join";
    }
}

class Ping extends ClientMessage {
    @Override
    protected String type() {
        return "ping";
    }
    protected Object data() { return null; }
}

class Throttle extends ClientMessage {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object data() {
        return value;
    }

    @Override
    protected String type() {
        return "throttle";
    }
}

class CreateRace extends ClientMessage {
    public Join botId;

    public String trackName, password;
    public int carCount;

    public CreateRace(String botName, String botKey, String trackName, String password, int carCount){
        this.botId = new Join(botName, botKey);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String type() {
        return "createRace";
    }
    protected Object data() { return this; }

}

class JoinRace extends ClientMessage {
    public Join botId;

    public String trackName, password;
    public int carCount;

    public JoinRace(String botName, String botKey, String trackName, String password, int carCount){
        this.botId = new Join(botName, botKey);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String type() {
        return "joinRace";
    }
    protected Object data() { return this; }
}

class SwitchLane extends ClientMessage {
    public SwitchDirection direction;

    public SwitchLane(SwitchDirection direction) {
        this.direction = direction;
    }

    protected Object data(){
        return direction.getSwitchDirection();
    }

    @Override
    protected String type() {
        return "switchLane";
    }
}

enum SwitchDirection {
    SWITCH_DIRECTION_LEFT("Left"),
    SWITCH_DIRECTION_RIGHT("Right");

    private String switchValue;

    SwitchDirection(String switchValue){
        this.switchValue = switchValue;
    }

    public String getSwitchDirection(){
        return this.switchValue;
    }
}

class Turbo extends ClientMessage {
    String message;

    public Turbo(String message) {
        this.message = message;
    }

    protected Object data(){
        return message;
    }

    @Override
    protected String type() {
        return "turbo";
    }
}


