package noobbot;

import com.fasterxml.jackson.databind.ObjectMapper;
import serverMessages.ServerMessage;
import serverMessages.basic.Basic;
import serverMessages.carPositions.CarPositions;
import serverMessages.crash.Crash;
import serverMessages.dnf.Dnf;
import serverMessages.finish.Finish;
import serverMessages.gameEnd.GameEnd;
import serverMessages.gameInit.GameInit;
import serverMessages.gameStart.GameStart;
import serverMessages.join.JoinRx;
import serverMessages.lapFinished.LapFinished;
import serverMessages.spawn.Spawn;
import serverMessages.tournamentEnd.TournamentEnd;
import serverMessages.turboAvailable.TurboAvailable;
import serverMessages.yourCar.YourCar;

import java.io.*;
import java.net.Socket;

public class Connection {

    private ObjectMapper objectMapper = new ObjectMapper();

    private String host;
    private int port;


    public Connection(String host, int port) {
        this.host = host;
        this.port = port;
    }

    private Socket socket;

    public boolean connect(){
        System.out.println("Connecting to " + host + ":" + port);
        try {
            this.socket = new Socket(host, port);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private PrintWriter printWriter;
    private BufferedReader bufferedReader;

    public boolean setupWriter(){
        try {
            printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public boolean setupReader(){
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public String readLine(){
        try {
            return bufferedReader.readLine();
        } catch (IOException e){
            return null;
        }
    }

    public ServerMessage readMessage(){
        String line = readLine();

        if (line == null){
            return null;
        }

        Basic tmpMessage = null;
        try {
            tmpMessage = objectMapper.readValue(line, Basic.class);
        } catch (IOException e) {
            System.out.println("Failed to parse basic message type");
            System.out.println(line);
        }

        if (tmpMessage == null){
            return null;
        }


        switch (tmpMessage.getMsgType()){

            case "carPositions":
                return castToClass(line, CarPositions.class);

            case "yourCar":
                return castToClass(line, YourCar.class);

            case "gameInit":
                return castToClass(line, GameInit.class);

            case "gameEnd":
                return castToClass(line, GameEnd.class);

            case "dnf":
                return castToClass(line, Dnf.class);

            case "lapFinished":
                return castToClass(line, LapFinished.class);

            case "crash":
                return castToClass(line, Crash.class);

            case "finish":
                return castToClass(line, Finish.class);

            case "spawn":
                return castToClass(line, Spawn.class);

            case "turboAvailable":
                return castToClass(line, TurboAvailable.class);

            case "gameStart":
                return castToClass(line, GameStart.class);

            case "tournamentEnd":
            case "createRace":
            case "join":
            case "turboStart":
            case "turboEnd":
                //Known one liners / repeats, hacky...
                return tmpMessage;

            default:
                System.out.println("Found unknown message: " + tmpMessage.getMsgType());
                if (tmpMessage.getMsgType() != null){
                    System.out.println("Data: " + tmpMessage.getData().toString());
                }
                return tmpMessage;
        }
    }

    private ServerMessage castToClass(String json, Class<?> classType) {
        try {
            return (ServerMessage) objectMapper.readValue(json, classType);
        } catch (Exception e) {
            System.out.println("Message casting failed: " + classType.toString());
            return null;
        }
    }

    public void sendMessage(ClientMessage message) {
        printWriter.println(message.toJson());
        printWriter.flush();
    }

    public void closeConnection(){
        try {
            socket.close();
        } catch (IOException e) {
        }
    }
}
