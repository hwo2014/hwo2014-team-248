package noobbot;

import serverMessages.ServerMessage;
import serverMessages.basic.Basic;
import serverMessages.carPositions.CarPositions;
import serverMessages.crash.Crash;
import serverMessages.dnf.Dnf;
import serverMessages.gameInit.GameInit;
import serverMessages.gameInit.Piece;
import serverMessages.gameInit.Track;

import serverMessages.lapFinished.LapFinished;

import serverMessages.spawn.Spawn;
import serverMessages.yourCar.YourCar;


public class Controller {

    private Connection connection;


    public Controller(Connection connection) {
        this.connection = connection;
    }

    private Track track;
    private YourCar ourCar = null;
    private CarController carController = null;

    public void startRaceLogic(){

        int currentPiece = -1;
        double lastPieceDist = 0.0;
        double remainPieceDist = 0.0;

        int segmentIndex;
        int prevSegmentIndex = 0;
        double maxSegmentDriftAngle =0;

        PidAlgorithm pidAlgorithm = null;
        float PID_P = 2;
        float PID_I = 0.02f;
        float PID_D = 0;
        float targetVel = 5;

        int tick = 0;
        double resistanceToDrift = 0;
        double maxSpeed = 4;
        double nextMaxSpeed = 4;
        boolean turboAvailable = false;

        //tweeks
        double driftTweek = 1.2; // >1 more drift <1 less drift, Catch my drift?
        double deceleration = 0.02; //need to calculate this from analysing decel. looks it is linear ish *which is nice)


        ServerMessage message = null;
        while ((message = connection.readMessage()) != null) {
            switch (message.getClass().toString()){

                case "class serverMessages.carPositions.CarPositions":


                    CarPositions cp = (CarPositions) message;
                    ClientMessage clientMessage = carController.getAction(cp);

                    connection.sendMessage(clientMessage);

                    break;

                case "class serverMessages.gameEnd.GameEnd":
                    System.out.println("Game ended!");
                    carController.reset();
                    break;

                case "class serverMessages.gameStart.GameStart":
                    System.out.println("Game started!");

                    //reset distance variables
                    currentPiece = -1;
                    lastPieceDist = 0.0;
                    remainPieceDist = 0.0;

                    connection.sendMessage(new Throttle(1));
                    break;

                case "class serverMessages.lapFinished.LapFinished":
                    carController.parseLapCompleted((LapFinished) message);
                    break;

                case "class serverMessages.dnf.Dnf":
                    Dnf dnf = (Dnf) message;
                    System.out.println("DNF - Reason: " + dnf.getData().getReason());
                    break;

                case "class serverMessages.join.JoinRx":
                    System.out.println("Joined");
                    break;

                case "class serverMessages.turboAvailable.TurboAvailable":
                    if (carController.isOnTrack()){
                        carController.setTurboAvailable();
                    }
                    break;

                case "class serverMessages.basic.Basic":
                    System.out.println(((Basic) message).getMsgType());
                    break;

                case "class serverMessages.yourCar.YourCar":
                    ourCar = (YourCar) message;
                    carController = new CarController(ourCar.getColour());
                    break;


                case "class serverMessages.gameInit.GameInit":
                    System.out.println("Race init");
                    GameInit gameInit = (GameInit) message;

                    //init PID controller
                    pidAlgorithm = new PidAlgorithm(5, 0, PID_P, PID_I, PID_D);

                    track = gameInit.getData().getRace().getTrack();
                    track.constructSegments();

                    if (gameInit.getData().getRace().getRaceSession().getLaps() != null){
                        track.setTotalLaps(gameInit.getData().getRace().getRaceSession().getLaps());
                    }

                    carController.setTrack(track);

                    break;


                case "class serverMessages.crash.Crash":
                    System.out.println("Crash");
                    carController.parseCrashMessage((Crash) message);
                    break;
                case "class serverMessages.spawn.Spawn":
                    System.out.println("spawn");
                    carController.parseSpawnMessage((Spawn) message);
                    break;

                case "class serverMessages.finish.Finish":

                default:
                    System.out.println(message.getClass().toString());
                break;
            }

        }

        connection.closeConnection();
    }

    public void quickJoinRace(String bot, String key){
        connection.sendMessage(new Join(bot, key));
    }

    public void createRace(String bot, String key, String track, String password, int carCount){
        connection.sendMessage(new CreateRace(bot, key, track, password, carCount));
    }

    public void joinRace(String bot, String key, String track, String password, int carCount){
        connection.sendMessage(new JoinRace(bot, key, track, password, carCount));
    }

}