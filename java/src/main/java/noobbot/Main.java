package noobbot;

import java.io.*;
import java.net.Socket;

public class Main {

    public static void main(String... args) {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        boolean create = false;
        boolean join = false;

        if (args.length >= 6){//Track is required too
            if (args[4].equals("-c")){
                create = true;
            }
            if (args[4].equals("-j")){
                join = true;
            }
        }

        String trackName = null;
        int carCount = 1;
        String password = null;

        if (create || join){
            trackName = args[5];

            if (args.length >= 7){
                carCount = Integer.parseInt(args[6]);
            }
            if (args.length == 8){
                password = args[7];
            }
        }

        Connection connection = new Connection(host, port);

        if (!connection.connect()){
            System.out.println("Failed to connect!");
            return;
        }

        if (!connection.setupReader()){
            System.out.println("Failed to setup reader!");
        }

        if (!connection.setupWriter()){
            System.out.println("Failed to setup writer!");
        }

        Controller controller = new Controller(connection);

        if (create){
            controller.createRace(botName, botKey, trackName, password, carCount);
        } else if (join){
            controller.joinRace(botName, botKey, trackName, password, carCount);
        } else {
            controller.quickJoinRace(botName, botKey);
        }

        controller.startRaceLogic();
    }

}
