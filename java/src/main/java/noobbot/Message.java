package noobbot;

import serverMessages.ServerMessage;

class Message implements ServerMessage {
    public final String msgType;
    public final Object data;
    public Integer gameTick = null;

    public Message(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public Message(final String msgType, final Object data, final Integer gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }
}