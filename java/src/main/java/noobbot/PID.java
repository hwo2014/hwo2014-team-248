package noobbot;

/**
 * Created by tom on 20/04/2014.
 */
    /**
     * PID algorithm shamelessly copied (with permission) from Brett's PID algorithm for Arduino
     * and rewritten for Java. See:
     *
     * http://brettbeauregard.com/blog/2011/04/improving-the-beginners-pid-introduction/
     *
     * This Library is licensed under a GPLv3 License
     *
     * Interface for the PID algorithms.
     *
     * @author dg
     *
     */
    public interface PID
    {
        /**
         * Sets the operation mode for the algorithm.
         */
        public enum Mode
        {
            AUTOMATIC,

            MANUAL
        };

        /**
         * Determines whether the raise Output to increase the process variable
         * or lower Output to increase the process variable.
         */
        public enum Direction
        {
            /**
             * Algorithm will raise Output to increase process variable.
             */
            DIRECT,

            /**
             * Algorithm will lower Output to increase process variable.
             */
            REVERSE
        };

        /**
         * This, as they say, is where the magic happens.  Call this method from your loop control.
         * The method will decide for itself using current time whether a new PID output value
         * needs to be computed.
         *
         * @param inputValue The measured process variable.
         * @return true if a new output value is computed, otherwise false.
         */
        public boolean compute( float inputValue, int tick );

        /**
         * Retrieves the current value of Output that is set by compute().
         *
         * @return output
         */
        public float getOutput();

        /**
         * Allows the controller's dynamic performance to be adjusted.

         * @param Kp
         * @param Ki
         * @param Kd
         */
        public void setTunings( float Kp, float Ki, float Kd );

        /**
         * Sets the target valid.
         *
         * @param target
         */
        public void setTarget( float target );

        /**
         * Sets the period, in milliseconds, at which the calculation is performed.  Used in
         * compute() to determine if a new output needs to be calculated.
         *
         * @param newSampleTime sample time in milliseconds.
         */
        public void setSampleTime( int newSampleTime );

        /**
         * Set the min and max values of the output variable.
         *
         * @param min
         * @param max
         */
        public void setOutputLimits( float min, float max );

        /**
         * Allows the controller Mode to be set to manual or Automatic.
         * When the transition from manual to auto occurs, the controller is
         * automatically initialized
         *
         * @param mode
         */
        public void setMode( Mode mode );

        /**
         * The PID will either be connected to a DIRECT acting process (+Output leads
         * to +Input) or a REVERSE acting process(+Output leads to -Input.)  we need to
         * know which one, because otherwise we may increase the output when we should
         * be decreasing.
         */
        public void setControllerDirection( Direction direction );

        /* Status Functions*************************************************************
         * Just because you set the Kp=-1 doesn't mean it actually happened.  These
         * functions query the internal state of the PID.  They're here for display
         * purposes.  This are the functions the PID Front-end uses for example
         ******************************************************************************/
        public float getKp();

        public float getKi();

        public float getKd();

        public Mode getMode();

        public Direction getDirection();

        /**
         * Used by PID algorithm to get current time.  Defaults to System.currentTimeMillis().
         * Intended to be overridden by test apps to supply well-defined time intervals.
         *
         */

    }
