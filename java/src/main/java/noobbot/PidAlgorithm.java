package noobbot;

/**
 * Created by tom on 20/04/2014.
 */
/**
 *
 */

/**
 * PID algorithm shamelessly copied (with permission) from Brett's PID algorithm for Arduino
 * and rewritten for Java.
 *
 * http://brettbeauregard.com/blog/2011/04/improving-the-beginners-pid-introduction/
 *
 * This Library is licensed under a GPLv3 License
 *
 * @author DG
 *
 */
public class PidAlgorithm implements PID
{
    private float           myOutput;
    private float           mySetpoint;
    private boolean         inAuto;
    private int             lastTick;
    private int             sampleTime;
    private float           ki, kp, kd;
    private float           iTerm;
    private float           outMin, outMax;
    private float           lastInput;
    private Direction       controllerDirection;
    private float           dispKp;
    private float           dispKi;
    private float           dispKd;

    public PidAlgorithm(float Setpoint, int tick, float Kp, float Ki, float Kd)
    {
        mySetpoint = Setpoint;
        inAuto = false;

        setOutputLimits( 0, 1 ); // default output limit corresponds to the arduino pwm limits

        sampleTime = 1; // default Controller Sample Time is 1 tick

        setControllerDirection( Direction.DIRECT );
        setMode( Mode.AUTOMATIC );
        setTunings( Kp, Ki, Kd );

        lastTick = tick;
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#compute()
     */
    public boolean compute( float inputValue, int tick )
    {
        if ( !inAuto )
            return false;

        //long now = timeSupplier.currentTimeMillis();
        long timeChange = ( tick - lastTick );
        if ( timeChange >= sampleTime )
        {
            /* Compute all the working error variables */
            float input = inputValue;
            float error = mySetpoint - input;
            iTerm += ( ki * error );

            if ( iTerm > outMax )
                iTerm = outMax;
            else if ( iTerm < outMin )
                iTerm = outMin;
            float dInput = ( input - lastInput );

            /* Compute PID Output */
            float output = kp * error + iTerm - kd * dInput;

            if ( output > outMax )
                output = outMax;
            else if ( output < outMin )
                output = outMin;
            myOutput = output;

            /* Remember some variables for next time */
            lastInput = input;
            lastTick = tick;
            return true;
        }
        else
            return false;
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#setTunings(float, float, float)
     */
    public void setTunings( float Kp, float Ki, float Kd )
    {
        if ( Kp < 0 || Ki < 0 || Kd < 0 )
            return;

        dispKp = Kp;
        dispKi = Ki;
        dispKd = Kd;

        //float sampleTimeInSec = ( (float) sampleTime ) / 1000;

        kp = Kp;
        ki = Ki * sampleTime;
        kd = Kd / sampleTime;

        if ( controllerDirection == Direction.REVERSE )
        {
            kp = ( 0 - kp );
            ki = ( 0 - ki );
            kd = ( 0 - kd );
        }
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#setSampleTime(int)
     */
    public void setSampleTime( int newSampleTime )
    {
        if ( newSampleTime > 0 )
        {
            float ratio = (float) newSampleTime / (float) sampleTime;
            ki *= ratio;
            kd /= ratio;
            sampleTime = newSampleTime;
        }
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#setOutputLimits(float, float)
     */
    public void setOutputLimits( float min, float max )
    {
        if ( min >= max )
            return;
        outMin = min;
        outMax = max;

        if ( inAuto )
        {
            if ( myOutput > outMax )
                myOutput = outMax;
            else if ( myOutput < outMin )
                myOutput = outMin;

            if ( iTerm > outMax )
                iTerm = outMax;
            else if ( iTerm < outMin )
                iTerm = outMin;
        }
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#setMode(org.deeg.pid.PidAlgorithm.Mode)
     */
    public void setMode( Mode mode )
    {
        boolean newAuto = ( mode == Mode.AUTOMATIC );
        if ( newAuto == !inAuto )
        { /* we just went from manual to auto */
            initialize();
        }
        inAuto = newAuto;
    }

    /* Initialize()****************************************************************
     *  does all the things that need to happen to ensure a bumpless transfer
     *  from manual to automatic mode.
     ******************************************************************************/
    private void initialize()
    {
        iTerm = myOutput;
        lastInput = 0;
        if ( iTerm > outMax )
            iTerm = outMax;
        else if ( iTerm < outMin )
            iTerm = outMin;
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#setControllerDirection(org.deeg.pid.PidAlgorithm.Direction)
     */
    public void setControllerDirection( Direction direction )
    {
        if ( inAuto && direction != controllerDirection )
        {
            kp = ( 0 - kp );
            ki = ( 0 - ki );
            kd = ( 0 - kd );
        }
        controllerDirection = direction;
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#getKp()
     */
    public float getKp()
    {
        return dispKp;
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#getKi()
     */
    public float getKi()
    {
        return dispKi;
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#getKd()
     */
    public float getKd()
    {
        return dispKd;
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#getMode()
     */
    public Mode getMode()
    {
        return inAuto ? Mode.AUTOMATIC : Mode.MANUAL;
    }

    /* (non-Javadoc)
     * @see org.deeg.pid.PID#getDirection()
     */
    public Direction getDirection()
    {
        return controllerDirection;
    }

    public float getOutput()
    {
        return myOutput;
    }

    public void setTarget( float target )
    {
        mySetpoint = target;
    }
}
