package noobbot;

import serverMessages.gameInit.Piece;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tom on 22/04/2014.
 */
public class Segment {

    private ArrayList<Piece> segmentPieces = new ArrayList<>();

    private double length=0;
    private double radius=0;
    private double angle=0;


    private ArrayList<Piece> switchPieces = new ArrayList<>();

    private double boost=0;

    public void addPiece(Piece piece){
        segmentPieces.add(piece);
        if (piece.getSwitch()){
            switchPieces.add(piece);
        }
        if(piece.isBend()){
            radius = piece.getRadius();
            angle += piece.getAngle();
        }else{
            length += piece.getLength();
        }
    }

    public int getPieceIndex(Piece piece){
        return segmentPieces.indexOf(piece);
    }

    public int getNumberOfPieces(){ return segmentPieces.size(); }

    public Piece getFirstPiece(){ return segmentPieces.get(0); }

    public Piece getPieceAtIndex(int index){ return segmentPieces.get(index); }

    public Piece getLastPiece(){ return segmentPieces.get(segmentPieces.size()-1); }

    public boolean canSwitch(){return switchPieces.size() > 0;}

    public ArrayList<Piece> getSwitchPieces(){ return switchPieces;}

    public double getLength(){
        if(isBend()){
            return Math.abs(2 * (angle/360) * Math.PI * radius);
        }else{
            return length;
        }
    }

    public double getAdjustedLength(double distanceFromCenter){
        if(isBend()){
            double adjRadius = getAdjustedRadius(distanceFromCenter);
            return Math.abs(2 * (angle/360) * Math.PI * adjRadius);
        }else{
            return length;
        }
    }

    public double getLastLapLength(){
        double length = 0;
        for (Piece segmentPiece : segmentPieces) {
            if (segmentPiece.containsStartLine()){
                break;
            }
            length += segmentPiece.getLength();
        }
        return length;
    }

    public double getAdjustedRadius(double distanceFromCenter){
        if(isBend()){
            double adjRadius = angle < 0 ? radius + distanceFromCenter : radius - distanceFromCenter;
            return adjRadius;
        }else{
            return 0.0;
        }
    }

    public boolean isBend(){
        return radius > 0;
    }

    public int getDirection(){
        return Double.compare(angle, 0);
    }

    public double apex(){
        if(isBend()) {
            int noBends = segmentPieces.size();
            return (double) noBends / 2;
        }
        return 0;
    }

    public double getBoost(){
        return boost;
    }

    public void increaseBoost(double boostIncrease){
        boost += boostIncrease;
    }

    public double getAngle() { return angle; }
    public double getRadius() { return radius; }

    public void addToEnd(Segment add){
        segmentPieces.addAll(add.segmentPieces);
        for (int i = 0; i < add.segmentPieces.size(); i++) {
            add.segmentPieces.get(i).setSegment(this);
        }
        length += add.getLength();
    }

    public List<Piece> getPieces(){
        return segmentPieces;
    }

    public boolean containsStartLine(){
        //return segmentPieces.stream().anyMatch(Piece::containsStartLine); java8 not enabled?
        for (Piece p: segmentPieces){
            if (p.containsStartLine()){
                return true;
            }
        }
        return false;
    }

    //Checks if piece is before the start line in current segment
    //inefficient but works
    public boolean isBeforeStartLine(Piece currentPiece){
        if (!containsStartLine()){
            return false;
        }

        for (Piece segmentPiece : segmentPieces) {
            if (segmentPiece.containsStartLine()){
                return false;
            } else if (segmentPiece.equals(currentPiece)){
                return true;
            }
        }
        return false;
    }

    public double distanceToSwitch(Piece piece, double distanceFromCenter){
        if (!canSwitch()){
            return -1;
        }
        if (segmentPieces.indexOf(piece) < 0){
            return -1;
        }

        boolean foundSwitch = false;

        List<Piece> toCheck = piecesAfter(piece);
        Segment fakeSegment = new Segment();
        double length = 0;
        for (Piece segmentPiece : toCheck) {
            if (segmentPiece.getSwitch()){
                foundSwitch = true;
                break;
            }
            fakeSegment.addPiece(segmentPiece);
        }
        if (foundSwitch){
            return fakeSegment.getAdjustedLength(distanceFromCenter);
        } else {
            //Already past switch?
            return -1;
        }
    }

    public double distanceToSwitchIncludingStart(double distanceFromCenter){
        if (getFirstPiece().getSwitch()){
            //Not really correct but hey..
            return 0;
        }

        double distance = distanceToSwitch(getFirstPiece(), distanceFromCenter);
        if (distance < 0){
            System.out.println("Error: including start.. distanceToSwitchFailed");
            return -1;
        } else {
            Segment fakeSegment = new Segment();
            fakeSegment.addPiece(getFirstPiece());
            distance += fakeSegment.getAdjustedLength(distanceFromCenter);
        }
        return distance;
    }

    public double distanceToEnd(Piece piece, double distanceFromCenter){
        if (segmentPieces.indexOf(piece) < 0){
            return -1;
        }

        List<Piece> toCheck = piecesAfter(piece);
        Segment fakeSegment = new Segment();
        double length = 0;
        for (Piece segmentPiece : toCheck) {
            fakeSegment.addPiece(segmentPiece);
        }
        return fakeSegment.getAdjustedLength(distanceFromCenter);
    }

    private List<Piece> piecesAfter(Piece p){
        int index;
        for (index = 0; index < segmentPieces.size(); index++) {
            if (p.equals(segmentPieces.get(index))){
                index++;
                break;
            }
        }
        // Couldn't remember how subList works, one is inclusive, one exclusive
        //return segmentPieces.subList(index + 1, segmentPieces.size() - 1);

        List<Piece> pieces = new ArrayList<>();
        for (index = index; index < segmentPieces.size(); index++) {
            pieces.add(segmentPieces.get(index));
        }
        return pieces;
    }

}


