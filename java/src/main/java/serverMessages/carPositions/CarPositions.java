
package serverMessages.carPositions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import serverMessages.ServerMessage;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "msgType",
    "data",
    "gameId",
    "gameTick"
})
public class CarPositions implements ServerMessage {

    public int getCarIdByColour(String colour){
        for (Datum car: data){
            if (car.getId().getColor().equals(colour)){
                return data.indexOf(car);
            }
        }
        return -1;
    }

    public double getAngleByIndex(int index){
        if (data.size() > index){
            Datum car = data.get(index);
            return car.getAngle();
        }
        return -1;
    }

    public int currentPiece(int index){
        if (data.size() > index){
            Datum car = data.get(index);
            return car.getPiecePosition().getPieceIndex();
        }
        return -1;
    }

    public int getCurrentLane(int index){
        if (data.size() > index){
            Datum car = data.get(index);
            return car.getPiecePosition().getLane().getEndLaneIndex();
        }
        return -1;
    }

    public double getPieceDistance(int index) { return data.get(index).getPiecePosition().getInPieceDistance(); }

    @JsonProperty("msgType")
    private String msgType;
    @JsonProperty("data")
    private List<Datum> data = new ArrayList<Datum>();
    @JsonProperty("gameId")
    private String gameId;
    @JsonProperty("gameTick")
    private Integer gameTick;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("msgType")
    public String getMsgType() {
        return msgType;
    }

    @JsonProperty("msgType")
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    @JsonProperty("data")
    public List<Datum> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Datum> data) {
        this.data = data;
    }

    @JsonProperty("gameId")
    public String getGameId() {
        return gameId;
    }

    @JsonProperty("gameId")
    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    @JsonProperty("gameTick")
    public Integer getGameTick() {
        return gameTick;
    }

    @JsonProperty("gameTick")
    public void setGameTick(Integer gameTick) {
        this.gameTick = gameTick;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
