
package serverMessages.carPositions;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "startLaneIndex",
    "endLaneIndex"
})
public class Lane {

    @JsonProperty("startLaneIndex")
    private Integer startLaneIndex;
    @JsonProperty("endLaneIndex")
    private Integer endLaneIndex;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("startLaneIndex")
    public Integer getStartLaneIndex() {
        return startLaneIndex;
    }

    @JsonProperty("startLaneIndex")
    public void setStartLaneIndex(Integer startLaneIndex) {
        this.startLaneIndex = startLaneIndex;
    }

    @JsonProperty("endLaneIndex")
    public Integer getEndLaneIndex() {
        return endLaneIndex;
    }

    @JsonProperty("endLaneIndex")
    public void setEndLaneIndex(Integer endLaneIndex) {
        this.endLaneIndex = endLaneIndex;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
