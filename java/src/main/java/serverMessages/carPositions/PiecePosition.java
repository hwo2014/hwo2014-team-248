
package serverMessages.carPositions;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "pieceIndex",
    "inPieceDistance",
    "lane",
    "lap"
})
public class PiecePosition {

    @JsonProperty("pieceIndex")
    private Integer pieceIndex;
    @JsonProperty("inPieceDistance")
    private Double inPieceDistance;
    @JsonProperty("lane")
    private Lane lane;
    @JsonProperty("lap")
    private Integer lap;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("pieceIndex")
    public Integer getPieceIndex() {
        return pieceIndex;
    }

    @JsonProperty("pieceIndex")
    public void setPieceIndex(Integer pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    @JsonProperty("inPieceDistance")
    public Double getInPieceDistance() {
        return inPieceDistance;
    }

    @JsonProperty("inPieceDistance")
    public void setInPieceDistance(Double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    @JsonProperty("lane")
    public Lane getLane() {
        return lane;
    }

    @JsonProperty("lane")
    public void setLane(Lane lane) {
        this.lane = lane;
    }

    @JsonProperty("lap")
    public Integer getLap() {
        return lap;
    }

    @JsonProperty("lap")
    public void setLap(Integer lap) {
        this.lap = lap;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
