package serverMessages.gameInit;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "length",
    "width",
    "guideFlagPosition"
})
public class Dimensions {

    @JsonProperty("length")
    private double length;
    @JsonProperty("width")
    private double width;
    @JsonProperty("guideFlagPosition")
    private double guideFlagPosition;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("length")
    public double getLength() {
        return length;
    }

    @JsonProperty("length")
    public void setLength(double length) {
        this.length = length;
    }

    @JsonProperty("width")
    public double getWidth() {
        return width;
    }

    @JsonProperty("width")
    public void setWidth(double width) {
        this.width = width;
    }

    @JsonProperty("guideFlagPosition")
    public double getGuideFlagPosition() {
        return guideFlagPosition;
    }

    @JsonProperty("guideFlagPosition")
    public void setGuideFlagPosition(double guideFlagPosition) {
        this.guideFlagPosition = guideFlagPosition;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
