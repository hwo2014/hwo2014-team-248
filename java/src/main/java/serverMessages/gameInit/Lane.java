package serverMessages.gameInit;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "distanceFromCenter",
    "index"
})
public class Lane {

    @JsonProperty("distanceFromCenter")
    private Integer distanceFromCenter;
    @JsonProperty("index")
    private Integer index;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("distanceFromCenter")
    public Integer getDistanceFromCenter() {
        return distanceFromCenter;
    }

    @JsonProperty("distanceFromCenter")
    public void setDistanceFromCenter(Integer distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
    }

    @JsonProperty("index")
    public Integer getIndex() {
        return index;
    }

    @JsonProperty("index")
    public void setIndex(Integer index) {
        this.index = index;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
