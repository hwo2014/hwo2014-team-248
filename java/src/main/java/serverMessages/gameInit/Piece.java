
package serverMessages.gameInit;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import noobbot.Segment;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "length",
    "switch",
    "radius",
    "angle"
})
public class Piece {
    transient Segment segment;
    transient static final int LEFT = -1;
    transient static final int RIGHT = 1;

    public boolean isBend(){
        return radius > 0;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public Segment getSegment(){
        return segment;
    }

    public int getDirection(){
        return Double.compare(angle, 0);
    }

    private boolean startPiece = false;

    public boolean containsStartLine(){
        return startPiece;
    }

    public void setStart(){
        startPiece = true;
    }

    @JsonProperty("length")
    private double length;
    @JsonProperty("switch")
    private Boolean _switch = false;
    @JsonProperty("radius")
    private Integer radius = 0;
    @JsonProperty("angle")
    private double angle = 0.0;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("length")
    public double getLength() {
        return length;
    }

    @JsonProperty("length")
    public void setLength(double length) {
        this.length = length;
    }

    @JsonProperty("switch")
    public Boolean getSwitch() {
        return _switch;
    }

    @JsonProperty("switch")
    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

    @JsonProperty("radius")
    public Integer getRadius() {
        return radius;
    }

    @JsonProperty("radius")
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    @JsonProperty("angle")
    public double getAngle() {
        return angle;
    }

    @JsonProperty("angle")
    public void setAngle(double angle) {
        this.angle = angle;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
