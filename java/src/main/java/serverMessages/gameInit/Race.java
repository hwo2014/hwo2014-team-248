package serverMessages.gameInit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "track",
    "cars",
    "raceSession"
})
public class Race {

    @JsonProperty("track")
    private Track track;
    @JsonProperty("cars")
    private List<Car> cars = new ArrayList<Car>();
    @JsonProperty("raceSession")
    private RaceSession raceSession;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("track")
    public Track getTrack() {
        return track;
    }

    @JsonProperty("track")
    public void setTrack(Track track) {
        this.track = track;
    }

    @JsonProperty("cars")
    public List<Car> getCars() {
        return cars;
    }

    @JsonProperty("cars")
    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    @JsonProperty("raceSession")
    public RaceSession getRaceSession() {
        return raceSession;
    }

    @JsonProperty("raceSession")
    public void setRaceSession(RaceSession raceSession) {
        this.raceSession = raceSession;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
