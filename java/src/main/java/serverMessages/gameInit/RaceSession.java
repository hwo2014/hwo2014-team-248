package serverMessages.gameInit;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "laps",
    "maxLapTimeMs",
    "quickRace"
})
public class RaceSession {

    @JsonProperty("laps")
    private Integer laps;
    @JsonProperty("maxLapTimeMs")
    private Integer maxLapTimeMs;
    @JsonProperty("quickRace")
    private Boolean quickRace;
    @JsonProperty("durationMs")
    private Integer durationMs;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("durationMs")
    public Integer getDurationMs() {
        return durationMs;
    }

    @JsonProperty("durationMs")
    public void setDurationMs(Integer durationMs) {
        this.durationMs = durationMs;
    }

    @JsonProperty("laps")
    public Integer getLaps() {
        return laps;
    }

    @JsonProperty("laps")
    public void setLaps(Integer laps) {
        this.laps = laps;
    }

    @JsonProperty("maxLapTimeMs")
    public Integer getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    @JsonProperty("maxLapTimeMs")
    public void setMaxLapTimeMs(Integer maxLapTimeMs) {
        this.maxLapTimeMs = maxLapTimeMs;
    }

    @JsonProperty("quickRace")
    public Boolean getQuickRace() {
        return quickRace;
    }

    @JsonProperty("quickRace")
    public void setQuickRace(Boolean quickRace) {
        this.quickRace = quickRace;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
