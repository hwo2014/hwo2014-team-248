package serverMessages.gameInit;

import java.time.Period;
import java.util.*;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import noobbot.*;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "id",
    "name",
    "pieces",
    "lanes",
    "startingPoint"
})
public class Track {

    transient ArrayList<Segment> segments = new ArrayList<>();

    int pieceIndex = -1;
    boolean pieceChanged = false;

    int prevSegmentIndex = -1;
    int segmentIndex = 0;
    Segment currentSegment = null;
    boolean segmentChanged = false;

    public void constructSegments(){
        Segment currCorner = null;
        Segment currStraight = null;
        double prevRadius = 0;
        int prevDirection=0;
        double prevLength =0;

        for (int i = 0; i < pieces.size(); i++) {
            Piece piece = pieces.get(i);
            if( piece.isBend() ){
                prevLength=0;
                if(prevRadius != piece.getRadius() || prevDirection != piece.getDirection()) {
                    currCorner = new Segment();
                    segments.add(currCorner);
                }
                if(currCorner !=null) {
                    currCorner.addPiece(piece);
                    piece.setSegment(currCorner);
                    prevRadius = piece.getRadius();
                    prevDirection = piece.getDirection();
                }
            }else{
                prevRadius = 0;
                prevDirection=0;
                if(prevLength==0) {
                    currStraight = new Segment();
                    segments.add(currStraight);
                    currStraight.addPiece(piece);
                    piece.setSegment(currStraight);
                    prevLength= piece.getLength();
                }else{
                    currStraight.addPiece(piece);
                    piece.setSegment(currStraight);
                    prevLength= piece.getLength();
                }
            }
        }
        Segment last = segments.get(segments.size()-1);
        Segment first = segments.get(0);
        if(last.getAngle()==first.getAngle() && last.getRadius() == first.getRadius()){
            last.addToEnd(first);
            segments.remove(first);
            segments.trimToSize();
        }

        /*Segment first = segments.get(0);
        Segment last = segments.get(segments.size() - 1);

        if (!first.isBend() && !last.isBend()){
            List<Piece> replacementPieces = new ArrayList<>();
            replacementPieces.addAll(last.getPieces());
            replacementPieces.addAll(first.getPieces());

            Segment replacement = new Segment();
            for (Piece piece: replacementPieces){
                piece.setSegment(replacement);
                replacement.addPiece(piece);
            }
            segments.remove(last);
            segments.add(0, replacement);
            segments.trimToSize();
        }*/
    }

    public void updatePiece(int pieceIndex){
        pieceChanged = this.pieceIndex != pieceIndex;
        if(pieceChanged){
            this.pieceIndex = pieceIndex;
            segmentIndex = getSegmentIndex(pieceIndex);
            currentSegment = getSegmentByIndex(segmentIndex);
            segmentChanged = (prevSegmentIndex != -1 && prevSegmentIndex != segmentIndex);
            prevSegmentIndex = segmentIndex;
        }else{
            segmentChanged=false;
        }

    }

    public boolean pieceChanged(){ return pieceChanged; }

    public boolean segmentChanged(){ return segmentChanged; }

    public double getDistanceFromCenter(int laneIndex){
        return lanes.get(laneIndex).getDistanceFromCenter();
    }
    

    //Segment Methods
    public boolean isCurrentSegmentBend(){ return currentSegment.isBend(); }
    public int getCurrentSegmentDirection(){ return currentSegment.getDirection(); }
    public double getCurrentSegmentAdjustedRadius(int lane){ return currentSegment.getAdjustedRadius(getDistanceFromCenter(lane)); }
    public double getCurrentSegmentAdjustedLength(int lane){ return currentSegment.getAdjustedLength(getDistanceFromCenter(lane)); }

    public Segment getNextSegment(){
        return segments.get(getNextSegmentIndex());
    }

    public int getNextSegmentIndex(){
        int nextSegmentIndex = segmentIndex+1 < segments.size() ? segmentIndex+1 : 0;
        return nextSegmentIndex;
    }

    public int getSegmentIndex(Segment segment){
        return segments.indexOf(segment);
    }

    public Segment getNextCornerSegment(){
        int nextCornerSegmentIndex = segmentIndex;
        do {
            nextCornerSegmentIndex = nextCornerSegmentIndex + 1 < segments.size() ? nextCornerSegmentIndex + 1 : 0;
        }while(!isSegmentBend(nextCornerSegmentIndex));
        return getSegmentByIndex(nextCornerSegmentIndex);
    }
    public double getDistToNextCornerSegment(){
        int nextCornerSegmentIndex = segmentIndex + 1 < segments.size() ? segmentIndex + 1 : 0;
        double length = 0.0;
        while(!isSegmentBend(getNextSegmentIndex(nextCornerSegmentIndex))){
            length += getSegmentByIndex(nextCornerSegmentIndex).getLength();
            nextCornerSegmentIndex = nextCornerSegmentIndex + 1 < segments.size() ? nextCornerSegmentIndex + 1 : 0;

        }
        return length;
    }

    public boolean currentSegmentApexReached(double segmentDistance, int lane){
        double cornerLength = getCurrentSegmentAdjustedLength(lane);
        if(getCurrentSegmentDirection() == getNextSegment().getDirection()){
            cornerLength += getNextSegment().getAdjustedLength(lane);
        }
        if(segmentDistance > 0.5 * cornerLength){
            //System.out.println("Apex Reached: "+ segmentDistance + " > " + 0.5 * cornerLength );
            return true;
        }

        return false;
    }

    public Segment getSegmentByPieceIndex(int pieceIndex){
        return pieces.get(pieceIndex).getSegment();
    }

    public Segment getSegmentByIndex(int segmentIndex){
        return segments.get(segmentIndex);
    }

    public int getSegmentIndex(int pieceIndex){
        return segments.indexOf(getSegmentByPieceIndex(pieceIndex));
    }

    public int getNextSegmentIndex(int pieceIndex) {
        int segmentIndex = segments.indexOf(getSegmentByPieceIndex(pieceIndex));
        if(segmentIndex+1 == segments.size()){
            return 0;
        }else{
            return segmentIndex+1;
        }
    }

    public boolean isSegmentBend(int segmentIndex){
        return segments.get(segmentIndex).isBend();
    }
    
    public double getSegmentAdjustedRadius(int segmentIndex, int laneIndex){
        return segments.get(segmentIndex).getAdjustedRadius(getDistanceFromCenter(laneIndex));
    }

    public double getSegmentAdjustedLength(int segmentIndex, int laneIndex){
        return segments.get(segmentIndex).getAdjustedLength(getDistanceFromCenter(laneIndex));
    }

    public List<Segment> getLongestSegments(){
        List<Segment> longestStraights = new ArrayList();
        for (Segment segment: segments){
            if (!segment.isBend()){
                longestStraights.add(segment);
            }
        }

        Collections.sort(longestStraights, new Comparator<Segment>() {
            public int compare(Segment o1, Segment o2) {
                if (o1.getLength() == o2.getLength()){
                    return 0;
                }
                if (isFinalLdap()){
                    return o1.getLastLapLength() > o2.getLastLapLength() ? -1 : 1;
                }
                return o1.getLength() > o2.getLength() ? -1 : 1;
            }
        });

        return longestStraights;
    }

    public int getPieceIndex(Piece piece){
        return pieces.indexOf(piece);
    }

    public Segment getNextSegment(Segment currentSegment){
        int endOfSegemnt = getPieceIndex(currentSegment.getLastPiece());
        if (endOfSegemnt < 0){
            return null;
        }

        Segment nextSegment = getSegmentByPieceIndex(getNextPieceByIndex(endOfSegemnt));
        return nextSegment;
    }

    private int getNextPieceByIndex(int pieceIndex){
        if ((pieceIndex + 1) < pieces.size()){
            return pieceIndex + 1;
        } else {
            return 0;
        }
    }

    public double getDistanceOfFirstCorners(int lane){
        double distanceFromCenter = getLanes().get(lane).getDistanceFromCenter();

        double distance = 0;
        Segment segment = getPieces().get(getNextCornerPiece(getPieceIndex(getCurrentPiece()))).getSegment();
        distance += segment.getAdjustedLength(distanceFromCenter);

        while ((segment = getNextSegment(segment)).isBend()){
            distance += segment.getAdjustedLength(distanceFromCenter);
        }
        return distance;
    }

    public Piece getFirstSwitch(){
        for (Piece piece : pieces) {
            if (piece.getSwitch()){
                return piece;
            }
        }
        return null;
    }

    public Piece getNextPiece(){
        return pieces.get(getNextPieceByIndex(pieceIndex));
    }

    public Piece getNextPiece(int fromIndex){
        return pieces.get(getNextPieceByIndex(fromIndex));
    }

    //Taking the title of most.. errm.. interesting method from construct segments...
    //A few methods have been tested :)
    public double distanceToSwitch(Piece fromPiece, int lane){
        Segment currentSegment = fromPiece.getSegment();
        double distanceTo;
        double distanceFromCenter = getLanes().get(lane).getDistanceFromCenter();

        //Current segment distance
        if (currentSegment.canSwitch()){
            distanceTo = currentSegment.distanceToSwitch(fromPiece, distanceFromCenter);
            if (distanceTo > 0){
                return distanceTo;
            } else {
                //Already past the switch in this segment
                distanceTo = currentSegment.distanceToEnd(fromPiece, distanceFromCenter);
                if (distanceTo < 0){
                    System.out.println("Something has gone wrong... piece.getSegment() in incorrect?");
                    return -1;
                }
            }
        } else {
            //Can't switch in this segment
            distanceTo = currentSegment.distanceToEnd(fromPiece, distanceFromCenter);
            if (distanceTo < 0){
                System.out.println("Something has gone wrong... piece.getSegment() in incorrect?");
                return -1;
            }
        }

        //All other segments...
        boolean foundSwitch = false;
        while (!foundSwitch){
            currentSegment = getNextSegment(currentSegment);

            if (currentSegment.canSwitch()){
                distanceTo += currentSegment.distanceToSwitchIncludingStart(distanceFromCenter);
                foundSwitch = true;
            } else {
                distanceTo += currentSegment.getAdjustedLength(distanceFromCenter);
            }

            if (fromPiece.getSegment().equals(currentSegment) && !foundSwitch){
                //we've done a complete loop of the track and not found anything exiting inf loop.
                System.out.println("Something has gone wrong, no switches found, exiting inf loop!");
                break;
            }

        }

        return distanceTo;
    }

    public Piece getNextSwitchOld(Piece currentSwitch){
        boolean search = false;
        for (Piece piece : pieces) {
            if (currentSwitch.equals(piece)){
                search = true;
                continue;
            }

            if (search && piece.getSwitch()){
                return piece;
            }

            if (search && currentSwitch.equals(piece)){
                return currentSwitch;
            }
        }
        return null;
    }

    public Piece getNextSwitch(Piece currentSwitch){
        boolean search = false;

        for (Piece piece : pieces) {
            if (currentSwitch.equals(piece)){
                search = true;
                continue;
            }

            if (search && piece.getSwitch()){
                return piece;
            }

            if (search && currentSwitch.equals(piece)){
                return currentSwitch;
            }
        }
        return null;
    }

        //Piece Methods
    public double getCurrentPieceDistance(int lane){ return getPieceDistance(pieceIndex,lane); }
    public double getCurrentPieceDistanceRem(double currentDistance, int lane){ return getPieceDistanceRemaining(pieceIndex, currentDistance, lane); }
    public double getCurrentPieceRadius(int lane){ return getPieceRadius(pieceIndex, lane); }
    public double getCurrentPieceAngle(){ return getPieceAngle(pieceIndex); }
    
    public double getPieceDistance(int pieceIndex, int lane){
        Piece piece = pieces.get(pieceIndex);
        if (piece.isBend()){
            //forgetting that you can switch on some corners
            //distance is * 2 * pie * (360/angle) * (bend radius + lane distance from Center)
            double angle = piece.getAngle();
            double radius = getPieceRadius(pieceIndex, lane); // lane adjusted radius

            double pieceDistance = Math.abs(2 * (angle/360) * Math.PI * radius);

            return pieceDistance;

        }else{
            return piece.getLength();
        }
    }

    public double getPieceDistanceRemaining(int pieceIndex, double currDistance, int lane){
        Piece piece = pieces.get(pieceIndex);
        if (piece.isBend()){
            double pieceDistance = getPieceDistance(pieceIndex,lane);
            return pieceDistance - currDistance;
        }else{
            return piece.getLength() - currDistance;
        }
    }

    public double getPieceRadius(int pieceIndex, int laneIndex){
        Piece piece = pieces.get(pieceIndex);
        if (piece.isBend()){
            double r = piece.getRadius();
            double distanceFromCenter = getLanes().get(laneIndex).getDistanceFromCenter();
            // could have use piece diecrtion method here instead
            double angle = getPieceAngle(pieceIndex);
            r = angle < 0 ? r + distanceFromCenter : r - distanceFromCenter;
            return r;
        }else{
            return 0.0;
        }
    }

    public double getPieceAngle(int pieceIndex){
        Piece piece = pieces.get(pieceIndex);
        if (piece.isBend()){
            return piece.getAngle();
        }else{
            return 0.0;
        }
    }

    public int getNextCornerPiece(int pieceIndex){
        int start = pieceIndex==pieces.size()-1 ? 0 : pieceIndex+1;
        for (int i = start; i < pieces.size(); i++) {
            if(i==pieceIndex){
                return pieceIndex;
            }
            if(pieces.get(i).isBend()){
                return i;
            }
            if(i==pieces.size()-1){
                i=0;
            }

        }
        return pieceIndex;
    }

    public double distanceToNextBend(int pieceIndex){
        int cornerIdx = getNextCornerPiece(pieceIndex);
        double length= 0;
        if(cornerIdx<pieceIndex){
            for (int i = 0; i < cornerIdx; i++) {
                Piece piece = pieces.get(i);
                if (!piece.isBend()) {
                    length += piece.getLength();
                }
            }
            cornerIdx = pieces.size();
        }
        for (int i = pieceIndex; i < cornerIdx; i++) {
            Piece piece = pieces.get(i);
            if (!piece.isBend()) {
                length += piece.getLength();
            }
        }
        return length;
    }

    private int totalLaps;
    private int lapsCompleted = 0;

    public void setTotalLaps(int laps){
        totalLaps = laps;
    }

    public int getLap(){
        return lapsCompleted;
    }

    public void lapCompleted(){
        lapsCompleted++;
    }

    public boolean isFinalLdap(){
        return lapsCompleted == (totalLaps - 1);
    }

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("pieces")
    private List<Piece> pieces = new ArrayList<Piece>();
    @JsonProperty("lanes")
    private List<Lane> lanes = new ArrayList<Lane>();
    @JsonProperty("startingPoint")
    private StartingPoint startingPoint;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("pieces")
    public List<Piece> getPieces() {
        return pieces;
    }

    @JsonProperty("pieces")
    public void setPieces(List<Piece> pieces) {
        this.pieces = pieces;
    }

    @JsonProperty("lanes")
    public List<Lane> getLanes() {
        return lanes;
    }

    @JsonProperty("lanes")
    public void setLanes(List<Lane> lanes) {
        this.lanes = lanes;
    }

    @JsonProperty("startingPoint")
    public StartingPoint getStartingPoint() {
        return startingPoint;
    }

    @JsonProperty("startingPoint")
    public void setStartingPoint(StartingPoint startingPoint) {
        this.startingPoint = startingPoint;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Piece getCurrentPiece() {
        return getPieces().get(this.pieceIndex);
    }



}
