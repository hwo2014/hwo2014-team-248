
package serverMessages.lapFinished;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "car",
    "lapTime",
    "raceTime",
    "ranking"
})
public class Data {

    @JsonProperty("car")
    private Car car;
    @JsonProperty("lapTime")
    private LapTime lapTime;
    @JsonProperty("raceTime")
    private RaceTime raceTime;
    @JsonProperty("ranking")
    private Ranking ranking;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("car")
    public Car getCar() {
        return car;
    }

    @JsonProperty("car")
    public void setCar(Car car) {
        this.car = car;
    }

    @JsonProperty("lapTime")
    public LapTime getLapTime() {
        return lapTime;
    }

    @JsonProperty("lapTime")
    public void setLapTime(LapTime lapTime) {
        this.lapTime = lapTime;
    }

    @JsonProperty("raceTime")
    public RaceTime getRaceTime() {
        return raceTime;
    }

    @JsonProperty("raceTime")
    public void setRaceTime(RaceTime raceTime) {
        this.raceTime = raceTime;
    }

    @JsonProperty("ranking")
    public Ranking getRanking() {
        return ranking;
    }

    @JsonProperty("ranking")
    public void setRanking(Ranking ranking) {
        this.ranking = ranking;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
