
package serverMessages.lapFinished;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import serverMessages.ServerMessage;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "msgType",
    "data",
    "gameId",
    "gameTick"
})
public class LapFinished implements ServerMessage {

    @JsonProperty("msgType")
    private String msgType;
    @JsonProperty("data")
    private Data data;
    @JsonProperty("gameId")
    private String gameId;
    @JsonProperty("gameTick")
    private Integer gameTick;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("msgType")
    public String getMsgType() {
        return msgType;
    }

    @JsonProperty("msgType")
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    @JsonProperty("gameId")
    public String getGameId() {
        return gameId;
    }

    @JsonProperty("gameId")
    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    @JsonProperty("gameTick")
    public Integer getGameTick() {
        return gameTick;
    }

    @JsonProperty("gameTick")
    public void setGameTick(Integer gameTick) {
        this.gameTick = gameTick;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
