
package serverMessages.lapFinished;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "lap",
    "ticks",
    "millis"
})
public class LapTime {

    @JsonProperty("lap")
    private Integer lap;
    @JsonProperty("ticks")
    private Integer ticks;
    @JsonProperty("millis")
    private Integer millis;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("lap")
    public Integer getLap() {
        return lap;
    }

    @JsonProperty("lap")
    public void setLap(Integer lap) {
        this.lap = lap;
    }

    @JsonProperty("ticks")
    public Integer getTicks() {
        return ticks;
    }

    @JsonProperty("ticks")
    public void setTicks(Integer ticks) {
        this.ticks = ticks;
    }

    @JsonProperty("millis")
    public Integer getMillis() {
        return millis;
    }

    @JsonProperty("millis")
    public void setMillis(Integer millis) {
        this.millis = millis;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
