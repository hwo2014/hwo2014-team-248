
package serverMessages.lapFinished;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "overall",
    "fastestLap"
})
public class Ranking {

    @JsonProperty("overall")
    private Integer overall;
    @JsonProperty("fastestLap")
    private Integer fastestLap;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("overall")
    public Integer getOverall() {
        return overall;
    }

    @JsonProperty("overall")
    public void setOverall(Integer overall) {
        this.overall = overall;
    }

    @JsonProperty("fastestLap")
    public Integer getFastestLap() {
        return fastestLap;
    }

    @JsonProperty("fastestLap")
    public void setFastestLap(Integer fastestLap) {
        this.fastestLap = fastestLap;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
