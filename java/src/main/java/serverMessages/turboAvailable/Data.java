
package serverMessages.turboAvailable;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "turboDurationMilliseconds",
    "turboDurationTicks",
    "turboFactor"
})
public class Data {

    @JsonProperty("turboDurationMilliseconds")
    private Double turboDurationMilliseconds;
    @JsonProperty("turboDurationTicks")
    private Integer turboDurationTicks;
    @JsonProperty("turboFactor")
    private Double turboFactor;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("turboDurationMilliseconds")
    public Double getTurboDurationMilliseconds() {
        return turboDurationMilliseconds;
    }

    @JsonProperty("turboDurationMilliseconds")
    public void setTurboDurationMilliseconds(Double turboDurationMilliseconds) {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
    }

    @JsonProperty("turboDurationTicks")
    public Integer getTurboDurationTicks() {
        return turboDurationTicks;
    }

    @JsonProperty("turboDurationTicks")
    public void setTurboDurationTicks(Integer turboDurationTicks) {
        this.turboDurationTicks = turboDurationTicks;
    }

    @JsonProperty("turboFactor")
    public Double getTurboFactor() {
        return turboFactor;
    }

    @JsonProperty("turboFactor")
    public void setTurboFactor(Double turboFactor) {
        this.turboFactor = turboFactor;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
